﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using System.Windows.Interop;
using System.Drawing.Imaging;
using Microsoft.VisualBasic;
using System.ComponentModel;

namespace biometry1
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    /// 
  

    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private System.Drawing.Color[,] pixels;
        private PointCollection luminanceHistogramPoints = null;
        private PointCollection redHistogramPoints = null;
        private PointCollection greenHistogramPoints = null;
        private PointCollection blueHistogramPoints = null;
        public event PropertyChangedEventHandler PropertyChanged;
        bool histogramsShown = false;

        public PointCollection LuminanceHistogramPoints
        {
            get
            {
                return this.luminanceHistogramPoints;
            }
            set
            {
                if (this.luminanceHistogramPoints != value)
                {
                    this.luminanceHistogramPoints = value;
                    if (this.PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("LuminanceHistogramPoints"));
                    }
                }
            }
        }

        public PointCollection BlueHistogramPoints
        {
            get
            {
                return this.blueHistogramPoints;
            }
            set
            {
                if (this.blueHistogramPoints != value)
                {
                    this.blueHistogramPoints = value;
                    if (this.PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("BlueHistogramPoints"));
                    }
                }
            }
        }

        public PointCollection RedHistogramPoints
        {
            get
            {
                return this.redHistogramPoints;
            }
            set
            {
                if (this.redHistogramPoints != value)
                {
                    this.redHistogramPoints = value;
                    if (this.PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("RedHistogramPoints"));
                    }
                }
            }
        }

        public PointCollection GreenHistogramPoints
        {
            get
            {
                return this.greenHistogramPoints;
            }
            set
            {
                if (this.greenHistogramPoints != value)
                {
                    this.greenHistogramPoints = value;
                    if (this.PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("GreenHistogramPoints"));
                    }
                }
            }
        }
        public MainWindow()
        {
            InitializeComponent();

            this.DataContext = this;
        }

        private void loadFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Pliki zdjęć (*.png;*.jpeg)|*.png;*.jpeg;*.jpg";
            if (openFileDialog.ShowDialog() == true)
            {
                img.Source = new BitmapImage(new Uri(openFileDialog.FileName));
            }

            Bitmap bmp = BitmapImage2Bitmap((BitmapImage)img.Source);
            pixels = new System.Drawing.Color[bmp.Width, bmp.Height];

            for (int i = 0; i < bmp.Width; i++)
            {
                for (int j = 0; j < bmp.Height; j++)
                {
                    pixels[i, j] = bmp.GetPixel(i, j);
                }
            }

            if (histogramsShown)
                showHistogram(sender, e);

        }

        

        private void paintImage()
        {
            Bitmap bmp = BitmapImage2Bitmap((BitmapImage)img.Source);

            for (int i = 0; i < bmp.Width; i++)
            {
                for (int j = 0; j < bmp.Height; j++)
                {
                    bmp.SetPixel(i, j, pixels[i,j]);
                }
            }

            img.Source = Bitmap2BitmapImage(bmp);
        }

        private void toGrayscale(object sender, RoutedEventArgs e)
        {

            for(int i=0; i<pixels.GetLength(0); i++)
            {
                for(int j=0; j<pixels.GetLength(1); j++)
                {
                    int color = (pixels[i,j].R + pixels[i,j].G +
                        pixels[i,j].B) / 3;

                    pixels[i, j] = System.Drawing.Color.FromArgb(color, color, color);
                }
            }

            paintImage();

            if (histogramsShown)
                showHistogram(sender, e);
        }

        private void negation(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < pixels.GetLength(0); i++)
            {
                for (int j = 0; j < pixels.GetLength(1); j++)
                {

                   pixels[i, j] = System.Drawing.Color.FromArgb(255 - pixels[i, j].R, 255 - pixels[i, j].G, 
                        255 - pixels[i, j].B);
                }
            }

            paintImage();

            if (histogramsShown)
                showHistogram(sender, e);

        }

        private void brightness(object sender, RoutedEventArgs e)
        {
            int level;
            Int32.TryParse(Microsoft.VisualBasic.Interaction.InputBox("Proszę wpisać poziom jasności z przedziału [-255; 255].", "Jasność", "0"), out level);

            for (int i = 0; i < pixels.GetLength(0); i++)
            {
                for (int j = 0; j < pixels.GetLength(1); j++)
                {

                    int R = pixels[i,j].R + level;
                    if(R>255)
                    {
                        R = 255;
                    }
                    if(R<0)
                    {
                        R = 0;
                    }

                    int G = pixels[i, j].G + level;
                    if (G > 255)
                    {
                        G = 255;
                    }
                    if (G < 0)
                    {
                        G = 0;
                    }

                    int B = pixels[i, j].B + level;
                    if (B > 255)
                    {
                        B = 255;
                    }
                    if (B < 0)
                    {
                        B = 0;
                    }

                    pixels[i, j] = System.Drawing.Color.FromArgb(R, G, B);
                }
            }

            paintImage();

            if (histogramsShown)
                showHistogram(sender, e);

        }


        private void contrast(object sender, RoutedEventArgs e)
        {
            double level;
            Double.TryParse(Microsoft.VisualBasic.Interaction.InputBox("Proszę wpisać poziom kontrastu > 0.", "Kontrast", "0"), out level);

            for (int i = 0; i < pixels.GetLength(0); i++)
            {
                for (int j = 0; j < pixels.GetLength(1); j++)
                {
                    int R = pixels[i, j].R;
                    double normalisedR = (255 * Math.Pow(((double) R / 255) , level));

                    int G = pixels[i, j].G;
                    double normalisedG = (255 * Math.Pow(((double)G / 255), level));

                    int B = pixels[i, j].B;
                    double normalisedB = (255 * Math.Pow(((double)B / 255), level));

                    pixels[i, j] = System.Drawing.Color.FromArgb((int) normalisedR,(int) normalisedG,(int) normalisedB);
                }
            }

            paintImage();

            if (histogramsShown)
                showHistogram(sender, e);

        }


        private void thresholding(object sender, RoutedEventArgs e)
        {
            int level;
            Int32.TryParse(Microsoft.VisualBasic.Interaction.InputBox("Proszę wpisać poziom progowania z przedziału [0, 255].", "Progowanie", "0"), out level);

            // assuming brightness  =  sqrt( .241 R2 + .691 G2 + .068 B2 ), then it is in range [0, 255]


            for (int i = 0; i < pixels.GetLength(0); i++)
            {
                for (int j = 0; j < pixels.GetLength(1); j++)
                {

                    int b = (int)Math.Sqrt(
                        pixels[i, j].R * pixels[i, j].R * .241 +
                        pixels[i, j].G * pixels[i, j].G * .691 +
                        pixels[i, j].B * pixels[i, j].B * .068);

                    if(b > level)
                    {
                        pixels[i, j] = System.Drawing.Color.FromArgb(255, 255, 255);
                    }

                   else
                    {
                        pixels[i, j] = System.Drawing.Color.FromArgb(0, 0, 0);
                    }
                }
            }

            paintImage();

            if (histogramsShown)
                showHistogram(sender, e);
        }

        private void normalizeHistogram(object sender, RoutedEventArgs e)
        {
            int minR = pixels[0, 0].R;
            int maxR = pixels[0, 0].R;

            int minG = pixels[0, 0].G;
            int maxG = pixels[0, 0].G;

            int minB = pixels[0, 0].B;
            int maxB = pixels[0, 0].B;

            for (int i = 0; i < pixels.GetLength(0); i++)
            {
                for (int j = 0; j < pixels.GetLength(1); j++)
                {
                    if (pixels[i, j].R < minR)
                        minR = pixels[i, j].R;
                    if (pixels[i, j].R > maxR)
                        maxR = pixels[i, j].R;

                    if (pixels[i, j].G < minG)
                        minG = pixels[i, j].G;
                    if (pixels[i, j].G > maxG)
                        maxG = pixels[i, j].G;

                    if (pixels[i, j].B < minB)
                        minB = pixels[i, j].B;
                    if (pixels[i, j].B > maxB)
                        maxB = pixels[i, j].B;

                }
            }

            for (int i = 0; i < pixels.GetLength(0); i++)
            {
                for (int j = 0; j < pixels.GetLength(1); j++)
                {
                    pixels[i, j] = System.Drawing.Color.FromArgb(255 * (pixels[i, j].R - minR) / (maxR - minR),
                        255 * (pixels[i, j].G - minG) / (maxG - minG), 255 * (pixels[i, j].B - minB) / (maxB - minB));
                }
            }

            paintImage();
            showHistogram(sender, e);
        }

        private void equalizeHistogram(object sender, RoutedEventArgs e)
        {
            int[] red = new int[256];
            int[] green = new int[256];
            int[] blue = new int[256];

            int[] redEqualized = new int[256];
            int[] greenEqualized = new int[256];
            int[] blueEqualized = new int[256];

            for (int i = 0; i < pixels.GetLength(0); i++)
            {
                for (int j = 0; j < pixels.GetLength(1); j++)
                {
                    red[pixels[i,j].R]++;
                    green[pixels[i,j].G]++;
                    blue[pixels[i,j].B]++;
                }
            }

            for(int i = 1; i<red.Length; i++)
            {
                red[i] += red[i - 1];
                green[i] += green[i - 1];
                blue[i] += blue[i - 1];
            }

            int averageRed = red[red.Length - 1] / 255;
            int restRed = red[red.Length - 1] % 255;

            int averageGreen = green[green.Length - 1] / 255;
            int restGreen = green[green.Length - 1] % 255;

            int averageBlue = blue[blue.Length - 1] / 255;
            int restBlue = red[blue.Length - 1] % 255;

            for (int i = 0; i < red.Length; i++)
            {
                redEqualized[i] += averageRed;
                greenEqualized[i] += averageGreen;
                blueEqualized[i] += averageBlue;
            }

            for(int i=0; i<restRed; i++)
            {
                redEqualized[i]++;
            }

            for (int i = 0; i < restGreen; i++)
            {
                greenEqualized[i]++;
            }

            for (int i = 0; i < restBlue; i++)
            {
                blueEqualized[i]++;
            }

            for (int i = 1; i < red.Length; i++)
            {
                redEqualized[i] += redEqualized[i - 1];
                greenEqualized[i] += greenEqualized[i - 1];
                blueEqualized[i] += blueEqualized[i - 1];
            }

            for (int i = 0; i < pixels.GetLength(0); i++)
            {
                for (int j = 0; j < pixels.GetLength(1); j++)
                {
                    int differenceR = Math.Abs(red[pixels[i, j].R] - redEqualized[pixels[i, j].R]);
                    int chosenR = pixels[i, j].R;
                    for(int k=0; k<redEqualized.Length; k++)
                    {
                        if (Math.Abs(red[pixels[i, j].R] - redEqualized[k]) < differenceR)
                        {
                            differenceR = Math.Abs(red[pixels[i, j].R] - redEqualized[k]);
                            chosenR = k;
                        }
                    }

                    int differenceG = Math.Abs(green[pixels[i, j].G] - greenEqualized[pixels[i, j].G]);
                    int chosenG = pixels[i, j].G;
                    for (int k = 0; k < greenEqualized.Length; k++)
                    {
                        if (Math.Abs(green[pixels[i, j].G] - greenEqualized[k]) < differenceG)
                        {
                            differenceG = Math.Abs(green[pixels[i, j].R] - greenEqualized[k]);
                            chosenG = k;
                        }
                    }

                    int differenceB = Math.Abs(blue[pixels[i, j].B] - blueEqualized[pixels[i, j].B]);
                    int chosenB = pixels[i, j].B;
                    for (int k = 0; k < blueEqualized.Length; k++)
                    {
                        if (Math.Abs(blue[pixels[i, j].B] - blueEqualized[k]) < differenceB)
                        {
                            differenceB = Math.Abs(blue[pixels[i, j].B] - blueEqualized[k]);
                            chosenB = k;
                        }
                    }

                    pixels[i, j] = System.Drawing.Color.FromArgb(chosenR, chosenG, chosenB);
                }
            }

            paintImage();
            showHistogram(sender, e);
        }

        private void verticalProjection(object sender, RoutedEventArgs e)
        {
            thresholding(sender, e);

            int lastWhite = 0;

            for (int i = 0; i < pixels.GetLength(0); i++)
            {
                for (int j = pixels.GetLength(1)-1; j >= 0; j--)
                {
                    if(pixels[i,j] == System.Drawing.Color.FromArgb(255,255,255))
                    {
                        if (lastWhite < j) lastWhite = j;
                    }
                    else
                    {
                        if(lastWhite > j)
                        {
                            pixels[i, lastWhite] = System.Drawing.Color.Black;
                            pixels[i, j] = System.Drawing.Color.White;
                            lastWhite--;
                        }
                    }
                }
            }

            paintImage();

            if (histogramsShown)
                showHistogram(sender, e);
        }

        private void horizontalProjection(object sender, RoutedEventArgs e)
        {
            thresholding(sender, e);

            int lastWhite = 0;

            for (int i = 0; i < pixels.GetLength(1); i++)
            {
                for (int j = pixels.GetLength(0)-1; j >= 0; j--)
                {
                    if(pixels[j,i] == System.Drawing.Color.FromArgb(255,255,255))
                    {
                        if (lastWhite < j) lastWhite = j;
                    }
                    else
                    {
                        if(lastWhite > j)
                        {
                            pixels[lastWhite, i] = System.Drawing.Color.Black;
                            pixels[j, i] = System.Drawing.Color.White;
                            lastWhite--;
                        }
                    }
                }
            }

            paintImage();

            if (histogramsShown)
                showHistogram(sender, e);
        }

        private void lowPass(object sender, RoutedEventArgs e)
        {
            int[,] mask = { { 1, 1, 1 }, { 1, 0, 1 }, { 1, 1, 1 } };

            applyFilter(mask);

            paintImage();

            if (histogramsShown)
                showHistogram(sender, e);

        }

        private void gauss(object sender, RoutedEventArgs e)
        {
            int[,] mask = { { 1, 4, 1 }, { 4, 12, 4 }, { 1, 4, 1 } };

            applyFilter(mask);

            paintImage();

            if (histogramsShown)
                showHistogram(sender, e);

        }

        private void sharpening(object sender, RoutedEventArgs e)
        {
            int[,] mask = { { 0, -1, 0 }, { -1, 4, -1 }, { 0, -1, 0 } };

            applyFilter(mask);

            paintImage();

            if (histogramsShown)
                showHistogram(sender, e);

        }

        private void edgeDetection(object sender, RoutedEventArgs e)
        {
            int[,] mask = { { 0, 0, 0 }, { 0, 1, 0 }, { 0, 0, -1 } };

            applyFilter(mask);

            paintImage();

            if (histogramsShown)
                showHistogram(sender, e);

        }


        private void applyFilter(int [,] mask)
        {
            int w = 0;

            for (int i = 0; i < mask.GetLength(0); i++)
                for (int j = 0; j < mask.GetLength(1); j++)
                    w += Math.Abs(mask[i, j]);

            int maxR = 0;
            int minR = 0;

            int maxG = 0;
            int minG = 0;

            int maxB = 0;
            int minB = 0;

            int[,] Rs = new int[pixels.GetLength(0), pixels.GetLength(1)];
            int[,] Gs = new int[pixels.GetLength(0), pixels.GetLength(1)];
            int[,] Bs = new int[pixels.GetLength(0), pixels.GetLength(1)];

            for (int i = 0; i < pixels.GetLength(0); i++)
            {
                for (int j = 0; j < pixels.GetLength(1); j++)
                {
                    int r = 0;
                    int g = 0;
                    int b = 0;
                    for (int k = -mask.GetLength(0) / 2; k <= mask.GetLength(0) / 2; k++)
                    {
                        for (int l = -mask.GetLength(1) / 2; l <= mask.GetLength(1) / 2; l++)
                        {
                            if (i + k >= 0 && j + l >= 0 && i + k < pixels.GetLength(0) && j + l < pixels.GetLength(1))
                            {
                                r += pixels[i + k, j + l].R * mask[k + 1, l + 1];
                                g += pixels[i + k, j + l].G * mask[k + 1, l + 1];
                                b += pixels[i + k, j + l].B * mask[k + 1, l + 1];
                            }
                        }
                    }
                    if (w != 0)
                    {
                        r /= w;
                        g /= w;
                        b /= w;
                        if (r > maxR)
                            maxR = r;
                        if (r < minR)
                            minR = r;
                        if (g > maxG)
                            maxG = g;
                        if (g < minG)
                            minG = g;
                        if (b > maxB)
                            maxB = b;
                        if (b < minB)
                            minB = b;
                    }
                    Rs[i, j] = r;
                    Gs[i, j] = g;
                    Bs[i, j] = b;
                }
            }
            for (int i = 0; i < pixels.GetLength(0); i++)
            {
                for (int j = 0; j < pixels.GetLength(1); j++)
                {
                    pixels[i, j] = System.Drawing.Color.FromArgb((Rs[i,j] + Math.Abs(minR))*255/(Math.Abs(maxR)+Math.Abs(minR)),
                        (Gs[i,j] + Math.Abs(minG)) * 255 / (Math.Abs(maxG) + Math.Abs(minG)), (Bs[i,j] + Math.Abs(minB)) * 255 / (Math.Abs(maxB) + Math.Abs(minB)));
                }
            }

        }

        private void showHistogram(object sender, RoutedEventArgs e)
        {
            int[] luminescence = new int[256];
            int[] red = new int[256];
            int[] green = new int[256];
            int[] blue = new int[256];

            PointCollection pkl = new PointCollection();
            PointCollection pkr = new PointCollection();
            PointCollection pkg = new PointCollection();
            PointCollection pkb = new PointCollection();

            for (int i = 0; i < pixels.GetLength(0); i++)
            {
                for (int j = 0; j < pixels.GetLength(1); j++)
                {

                    int b = (int)Math.Sqrt(
                        pixels[i,j].R * pixels[i,j].R * .241 +
                        pixels[i,j].G * pixels[i,j].G * .691 +
                        pixels[i,j].B * pixels[i,j].B * .068);

                    luminescence[b]++;
                    red[pixels[i,j].R]++;
                    green[pixels[i,j].G]++;
                    blue[pixels[i,j].B]++;
                }
            }

            pkl.Add(new System.Windows.Point(0, 255));
            pkr.Add(new System.Windows.Point(0, 255));
            pkg.Add(new System.Windows.Point(0, 255));
            pkb.Add(new System.Windows.Point(0, 255));
            for (int k = 0; k < luminescence.Length; k++)
            {
                pkl.Add(new System.Windows.Point(k, 255 - luminescence[k]));
                pkr.Add(new System.Windows.Point(k, 255 - red[k]));
                pkg.Add(new System.Windows.Point(k, 255 - green[k]));
                pkb.Add(new System.Windows.Point(k, 255 - blue[k]));
            }
            pkl.Add(new System.Windows.Point(luminescence.Length - 1, 255));
            pkr.Add(new System.Windows.Point(luminescence.Length - 1, 255));
            pkg.Add(new System.Windows.Point(luminescence.Length - 1, 255));
            pkb.Add(new System.Windows.Point(luminescence.Length - 1, 255));

            this.LuminanceHistogramPoints = pkl;
            this.RedHistogramPoints = pkr;
            this.GreenHistogramPoints = pkg;
            this.BlueHistogramPoints = pkb;

            histogramsShown = true;

        }


        private Bitmap BitmapImage2Bitmap(BitmapImage bitmapImage)
        {

            using (MemoryStream outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapImage));
                enc.Save(outStream);
                System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(outStream);

                return new Bitmap(bitmap);
            }
        }


        private BitmapImage Bitmap2BitmapImage(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, ImageFormat.Png);
                memory.Position = 0;
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();

                return bitmapImage;
            }
        }
    }

}
